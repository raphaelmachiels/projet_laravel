<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Critiques extends Model
{
    protected $fillable = ['vote', 'commentaire'];
    public function produit()
    {
        return $this->belongsTo('App\Produit');
    }
    public function user()
    {
        return $this->belongsTo('App\user');
    }
}
