<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateProduitRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' => 'required|min:3',
            'date'=> 'required|min:4|numeric',
            'synopsys' => 'required',
            'auteurs' => 'required|min:3',
            'cover' => 'required',
            'classement_id' => 'required|numeric',
            'duree' => 'required|min:2|numeric'
        ];
    }
}
