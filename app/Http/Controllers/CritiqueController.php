<?php

namespace App\Http\Controllers;

use App\Produit;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use \App\Critiques;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;


class CritiqueController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['only' => ['create', 'edit', 'update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CreateCritiqueRequest $request)
    {
        $donnees = $request->all();

        $critique = new \App\Critiques;


        if(Critiques::where('user_id', Auth::user()->id)->where('produit_id', $donnees['produit_id'])->get())
        {
            $critique->user_id = Auth::user()->id;
            $critique->produit_id = $donnees['produit_id'];
            $critique->vote = $donnees['vote'];
            $critique->commentaire = $donnees['commentaire'];
            $critique->save();
            \Session::flash('flash_message', 'Votre vote a bien été créé');
        }

        else
        {
            \Session::flash('flash_message', 'Votre vote n\'est pas créé');
        }



        return redirect('/produit/' . $donnees['produit_id']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        $userCritique = Critiques::where('user_id', $id)->first();

        $critique = Critiques::findOrFail($id);
        return view('albums.vote', compact('critique'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $critique = Critiques::findOrFail($id);
        return view('albums.vote', compact('critique'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $critique = Critiques::findOrFail($id);
        $critique->update($request->all());
        \Session::flash('flash_message', 'Votre critique a bien été modifiée');
        return view('albums.vote', compact('critique'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function showCritiques($user, $id)
    {

        $critiques = Critiques::where('user_id', $id)->get();


        return view('albums.showCritiques', compact('critiques'));
    }
    
}
