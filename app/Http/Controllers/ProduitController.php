<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Requests\CreateProduitRequest;
use \App\Produit;
use \App\Classement;
use \App\Critiques;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProduitController extends Controller
{


    public function __construct()
    {
        $this->middleware('admin', ['only' => ['create', 'store', 'edit', 'update']]);
//        $this->middleware('log', ['only' => ['fooAction', 'barAction']]);
//
//        $this->middleware('subscribed', ['except' => ['fooAction', 'barAction']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function welcome()
    {
        $produits = Produit::latest('created_at')->take(3)->get();


        foreach ($produits as $produit)
            $tabCritiques[] = $this->moyCritiques($produit->id);

        return view('welcome', compact('produits', 'tabCritiques', 'nbCritiques'));
    }

    public function index()
    {
        $produits = Produit::orderBy('nom', 'asc')->get();
        $classements = Classement::all();
        $search=false;
        return view('albums.showall', compact('produits', 'classements', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classements = \App\Classement::all();
        return view('albums.album', compact('classements'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProduitRequest $request)
    {
        $donnees = $request->all();

        $album = new \App\Produit;

        $album->nom = $donnees['nom'];
        $album->date = $donnees['date'];
        $album->synopsys = $donnees['synopsys'];
        $album->auteurs = $donnees['auteurs'];
        $album->cover = $donnees['cover'];
        $album->classement_id = $donnees['classement_id'];
        $album->duree = $donnees['duree'];

        \Session::flash('flash_message', 'L\'album à bien été ajouté à la base de données');

        $album->save();

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $moyenneCritiques = $this->moyCritiques($id);

        $classements = Classement::all();
        $produit = Produit::findOrFail($id);
        $nbCritiques = count($produit->critiques);
        $vote = true;

        if (Auth::user()) {
            $userCritique = Critiques::where('user_id', Auth::user()->id)->where('produit_id', $id)->first();
            if (count($userCritique) == 0)
                $vote = false;
        }

        return view('albums.show', compact('produit', 'classements', 'moyenneCritiques', 'vote', 'userCritique', 'nbCritiques'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produit = Produit::findOrFail($id);

        return view('albums.album', compact('produit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, CreateProduitRequest $request)
    {
        $produit = Produit::findOrFail($id);
        \Session::flash('flash_message', 'L\'album à bien été modifié dans la base de données');
        $produit->update($request->all());
        return view('albums.album', compact('produit'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Critiques::where('produit_id', $id)->delete();

        $produit = Produit::findOrFail($id);
        $produit->delete();

        $produits = Produit::orderBy('nom', 'asc')->get();
        $classements = Classement::all();
        return view('albums.showall', compact('produits', 'classements'));
    }

    public function vote($id)
    {
        $classements = Classement::all();
        $produit = Produit::findOrFail($id);

        return view('albums.vote', compact('produit', 'classements'));
    }

    public function search(Request $request)
    {

        $donnees = $request->all();

        $produits = DB::table('produits')
            ->where('nom', 'LIKE', '%' . $donnees['search'] . '%')
            ->orWhere('auteurs', 'LIKE', '%' . $donnees['search'] . '%')
            ->orderBy('date', 'desc')
            ->get();

        $search=false;
        
        if($produits != 0) {
            $search = true;
            $nbProduit = count($produits);
        }

        $classements = Classement::all();

        return view('albums.showall', compact('produits', 'classements', 'nbProduit', 'search'));

    }

    private function moyCritiques($id)
    {
        $nbCritiques = Critiques::where('produit_id', $id)->count('vote');
        if ($nbCritiques > 4) {
            $critiques = Critiques::where('produit_id', $id)->avg('vote');
        } else {
            $critiques = 0;
        }
        return $critiques;
    }


}