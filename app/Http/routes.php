<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'ProduitController@welcome');
 
Route::get('/produit/search', 'ProduitController@search');

Route::get('/produits/all', 'ProduitController@index');

Route::auth();

Route::get('/home', 'HomeController@index');

Route::resource('produit', 'ProduitController');

Route::get('/vote/{id}', 'ProduitController@vote');

Route::resource('critique', 'CritiqueController');

Route::get('/critiques/{user}/{id}', 'CritiqueController@showCritiques');