<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    protected $fillable = ['nom', 'date', 'synopsys', 'auteurs', 'cover', 'classement', 'duree'];

    public function critiques()
    {
        return $this->hasMany('App\Critiques');
    }
}
