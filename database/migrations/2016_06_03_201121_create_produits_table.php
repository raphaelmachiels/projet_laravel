<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('produits', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('nom', 50);
            $table->string('date', 4);
            $table->integer('classement_id')->unsigned();
            $table->string('duree', 3);
            $table->string('synopsys', 1000);
            $table->string('auteurs', 255);
            $table->string('cover', 50);
            $table->timestamps();
        });

        Schema::table('produits', function($table) { $table->foreign('classement_id')->references('id')->on('classements'); });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('produits');
    }
}
