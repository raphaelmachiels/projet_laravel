<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCritiquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('critiques', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('produit_id')->unsigned();
            $table->decimal('vote', 2, 1);
            $table->string('commentaire', 255);
            $table->timestamps();
        });
        Schema::table('critiques', function($table) { $table->foreign('produit_id')->references('id')->on('produits'); });
        Schema::table('critiques', function($table) { $table->foreign('user_id')->references('id')->on('users'); });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('critiques');
    }
}
