@extends('layouts.app')

@section('title', 'Accueil')


@section('content')
    <section id="index">
        <div class="container">
            @if(Session::has('flash_message'))
                <div class="alert alert-info up text-center">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_message') }}
                </div>
            @endif
            <div class="row background-box">
                @include('includes.search')
                <div class="col-xs-6 col-sm-6 col-sm-offset-3">
                    <div class="panel panel-default genre-heigh">
                        <div class="panel-body bg-primary">
                            <p class="line">
                            <span>
                                Actualité
                            <h1 class="text-center">
                                Nos dernières nouveautés
                            </h1>
                            </span>
                            </p>
                        </div>
                    </div>
                </div>
                <?php $no = 0 ?>
                @foreach($produits as $produit)

                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="panel panel-default text-infos">
                        <div class="panel-body">
                            <a href="/produit/{{ $produit->id }}"><img src="img/{{$produit->cover}}.jpg"
                                                                           class="img-responsive cover"></a>
                            <div class="line">
                                <span>
                                    <i class="fa fa-music" aria-hidden="true"></i>
                                    Artiste
                                </span>
                            </div>
                            <h1>{{$produit->auteurs}}</h1>
                            <div class="line">
                                <span>
                                    <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                    Album
                                </span>
                            </div>
                            <h2>{{$produit->nom}}</h2>
                            <div class="line">
                                <span>
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    Année
                                </span>
                            </div>
                            <h3>{{$produit->date}}</h3>
                            <div class="line">
                                <span>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                    Votes
                                </span>
                            </div>

                            <?php
                            $moyenneCritiques=$tabCritiques[$no];
                            ?>

                                @include ('includes.star')
                            <div class="clearfix"><p class="right">({{ $produit->critiques->count() }} votes)</p></div>
                            <?php $no = $no +1;?>

                            <hr>
                            <p class="heigh">
                                <?php $commentaire = $produit->synopsys ?>
                                {{ str_limit($commentaire, 100) }}
                            </p>
                            <a type="button" class="btn btn-default btn-block btn-lg"
                               href="/produit/{{ $produit->id }}">En savoir plus sur<br>{{ $produit->nom}}</a>

                        </div>
                    </div>
                </div>

            @endforeach

            </div>
        </div>
    </section>
@endsection
