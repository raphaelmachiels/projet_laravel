@extends('layouts.app')

@section('title', 'Album')
@section('content')
    <section id="show">
        <div class="container">
            @if(Session::has('flash_message'))
                <div class="alert alert-info up text-center">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_message') }}
                </div>
            @endif
            <div class="row background-box">
                @include('includes.search')
                <div class="col-xs-6 col-sm-3 col-sm-offset-1">
                    {{--<div class="panel panel-heading">--}}
                    <img src="/img/{{$produit->cover}}.jpg" class="img-responsive">
                    {{--</div>--}}
                </div>
                <div class="col-xs-6 col-sm-6 col-sm-offset-1">
                    <div class="panel panel-default genre-heigh">
                        <div class="panel-body">
                            <p class="line">
                            <span>
                                Genre
                                <h1 class="text-center">
                                    @foreach($classements as $classement)
                                        @if($classement->id == $produit->classement_id)
                                            <h1 class="text-center classement-size">{{ $classement->nom }}</h1>
                                        @endif
                                    @endforeach
                                </h1>
                            </span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-10 col-xs-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p class="line title">
                                <span>
                                    Synopsys
                                </span>
                            </p>
                            <p class="text-center">
                                {{ $produit->synopsys }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <a href="/" type="button" class="btn btn-default btn-block btn-lg space-bottom">
                        Accueil<br>
                        Mu<span class="logoMusic">z</span>i<span class="logoMusic">c</span>ity
                    </a>
                </div>
                <div class="col-xs-12 col-md-3">

                    @if(Auth::user())


                        @if(!$vote)

                            <a href="/vote/{{ $produit->id }}" type="button"
                               class="btn btn-default btn-block btn-lg space-bottom">
                                Ajouter une critique<br>
                                Mu<span class="logoMusic">z</span>i<span class="logoMusic">c</span>
                            </a>
                        @else
                            <a href="/critique/{{ $userCritique->id }}" type="button"
                               class="btn btn-default btn-block btn-lg space-bottom">
                                Modifier ma critique<br>
                                Mu<span class="logoMusic">z</span>i<span class="logoMusic">c</span>
                            </a>
                        @endif

                    @endif

                </div>
                <div class="col-xs-12 col-md-3">

                    @if(Auth::user())
                        <a href="/critiques/{{ Auth::user()->name }}/{{ Auth::user()->id }}" type="button"
                           class="btn btn-default btn-block btn-lg space-bottom">
                            Mes<br>
                            Cri<span class="logoMusic">t</span>i<span class="logoMusic">c</span>'s
                        </a>

                    @endif

                </div>
                <div class="col-xs-12 col-md-3">
                    <a href="/produits/all" type="button" class="btn btn-default btn-block btn-lg space-bottom">
                        Nos<br>
                        Mu<span class="logoMusic">z</span>i<span class="logoMusic">c</span>
                    </a>
                </div>
                @if(Auth::user())
                    @if($vote)
                        <div class="col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <p class="line title">
                                <span>
                                    Ma critique
                                </span>
                                    </p>


                                    <div class="clearfix">
                                        <li class="left">
                                            <p>{{ $userCritique->commentaire }}</p>
                                        </li>

                                        <p class="right">{{ date("d - M - Y", strtotime($userCritique->created_at)) }}</p>
                                    </div>
                                    <div class="right">Cri<span class="logoMusic">t</span>i<span
                                                class="logoMusic">c</span>
                                        de {{ $userCritique->user->name }}</div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif
                <div class="col-xs-12 col-sm-6 col-md-7 col-lg-8">
                    <div class="panel panel-default infos-album">
                        <div class="panel-body">
                            <p class="line-show">
                                <span>
                                    <i class="fa fa-music" aria-hidden="true"></i>
                                    Artiste
                                </span>
                            </p>
                            <h1>{{$produit->auteurs}}</h1>
                            <p class="line-show">
                                <span>
                                    <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                    Album
                                </span>
                            </p>
                            <h2>{{$produit->nom}}</h2>
                            <p class="line-show">
                                <span>
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    Année
                                </span>
                            </p>
                            <h3>{{$produit->date}}</h3>
                            <p class="line-show">
                                <span>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                    Votes
                                </span>
                            </p>
                            @include('includes.star')
                            <div class="clearfix">
                                <p class="right">
                                    ({{ $nbCritiques }}
                                    @if ($nbCritiques > 1)
                                        votes)
                                    @else
                                        vote)
                                    @endif
                                </p>
                            </div>
                            <p class="line-show">
                                <span>
                                    <i class="fa fa-hourglass-start" aria-hidden="true"></i>
                                    Durée
                                </span>
                            </p>
                            <h4>Durée : {{ $produit->duree }} min</h4>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">
                    <div class="panel panel-default infos-album">
                        <div class="panel-body">
                            <img src="/img/{{$produit->cover}}.jpg" class="img-responsive center">
                        </div>

                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @if(!$produit->critiques)

                                <p class="line title">
                                <span>
                                    Toutes les critiques
                                </span>
                                </p>
                            @endif
                            @foreach($produit->critiques as $critique)

                                <div class="clearfix">
                                    <li class="left">
                                        <p>{{ $critique->commentaire }}</p>
                                    </li>

                                    <p class="right">{{ date("d - M - Y", strtotime($critique->created_at)) }}</p>
                                </div>
                                <div class="right">Cri<span class="logoMusic">t</span>i<span
                                            class="logoMusic">c</span>
                                    de {{ $critique->user->name }}</div>
                                <hr>
                            @endforeach

                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <a href="/" type="button" class="btn btn-default btn-block btn-lg space-bottom">
                        Accueil<br>
                        Mu<span class="logoMusic">z</span>i<span class="logoMusic">c</span>ity
                    </a>
                </div>
                <div class="col-xs-12 col-md-3">

                    @if(Auth::user())


                        @if(!$vote)

                            <a href="/vote/{{ $produit->id }}" type="button"
                               class="btn btn-default btn-block btn-lg space-bottom">
                                Ajouter une critique<br>
                                Mu<span class="logoMusic">z</span>i<span class="logoMusic">c</span>
                            </a>
                        @else
                            <a href="/critique/{{ $userCritique->id }}" type="button"
                               class="btn btn-default btn-block btn-lg space-bottom">
                                Modifier ma critique<br>
                                Mu<span class="logoMusic">z</span>i<span class="logoMusic">c</span>
                            </a>
                        @endif

                    @endif

                </div>
                <div class="col-xs-12 col-md-3">

                    @if(Auth::user())
                        <a href="/critiques/{{ Auth::user()->name }}/{{ Auth::user()->id }}" type="button"
                           class="btn btn-default btn-block btn-lg space-bottom">
                            Mes<br>
                            Cri<span class="logoMusic">t</span>i<span class="logoMusic">c</span>'s
                        </a>

                    @endif

                </div>
                <div class="col-xs-12 col-md-3">
                    <a href="/produits/all" type="button" class="btn btn-default btn-block btn-lg space-bottom">
                        Nos<br>
                        Mu<span class="logoMusic">z</span>i<span class="logoMusic">c</span>
                    </a>
                </div>


            </div>
        </div>
    </section>
@endsection
