@extends('layouts.app')

@section('title', 'Tous les albums')
@section('content')
    <section id="showAllCritiques">
        <div class="container">

            <div class="row background-box">
                @include('includes.search')
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1>Toutes mes Cri<span class="logoMusic">t</span>i<span class="logoMusic">c</span>s</h1>
                        </div>
                        <div class="panel-body panel-primary">
                            @if (count($critiques) ==0)
                                <p class="text-center">Vous n'avez aucune critique, connectez-vous et votez!</p>
                            @endif

                            @foreach($critiques as $critique )
                                <div class="panel-heading text-center welcom">
                                    {{ $critique->produit->auteurs }}
                                </div>
                                <div>
                                    <li class="list-group-item clearfix">
                                        <div class="line">
                                <span>
                                    <i class="fa fa-commenting-o" aria-hidden="true"></i>
                                    Commentaire
                                </span>
                                        </div>

                                        <p>{{ $critique->commentaire }}</p>
                                        <div class="line">
                                <span>
                                    <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                    Album
                                </span>
                                        </div>
                                        <p>{{ $critique->produit->nom}}</p>
                                        <div class="line">
                                <span>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                    Votes
                                </span>
                                        </div>
                                        <p>
                                            {{ $critique->vote }}
                                            <i class="fa fa-star-o" aria-hidden="true"></i> sur <span>5</span>
                                        </p>
                                    </li>
                                </div>
                                <a href="/critique/{{$critique->id}}/edit" type="button"
                                   class="btn btn-default btn-block btn-lg space-bottom">
                                    Modifier cette critique<br>
                                    Mu<span class="logoMusic">z</span>i<span class="logoMusic">c</span>
                                </a>
                                <hr>
                            @endforeach


                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <a href="/" class="btn btn-default btn-block btn-lg space-bottom">
                        Accueil
                        Mu<span class="logoMusic">z</span>i<span class="logoMusic">c</span>ity
                    </a>
                </div>
                <div class="col-xs-12 col-md-4 col-md-offset-4">

                    <a href="{{ URL::previous() }}" class="btn btn-default btn-block btn-lg space-bottom">
                        Page Précédente
                        Mu<span class="logoMusic">z</span>i<span class="logoMusic">c</span>ity
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection
