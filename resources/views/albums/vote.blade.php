@extends('layouts.app')

@section('title', 'Album')
@section('content')

    <?php
    if (isset($critique))
        {
            $produit = $critique->produit;
        }
    ?>

    <section id="update-vote">
        <div class="container">
            @if(Session::has('flash_message'))
                <div class="alert alert-info up text-center">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_message') }}
                </div>
            @endif
            <div class="row background-box">
                @include('includes.search')
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-primary">
                        <div class="panel-heading text-center welcom">
                            <p class="line-white">
                            <span>
                                Album
                            </span>
                            </p>
                        </div>
                        <h1 class="text-center vote-size">
                            {{ $produit->nom }}
                        </h1>
                        <h2 style="text-align: center">Artiste : {{ $produit->auteurs }}</h2>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p class="line title">
                                <span>
                                    Synopsys
                                </span>
                            </p>
                            <p class="text-center">
                                {{ $produit->synopsys }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p class="line title">
                                <span>
                                    Critique
                                </span>
                            </p>

                            @if (isset($critique))
                                {!! Form::model($critique,['method'=>'PUT', 'url' => 'critique/'.$critique->id]) !!}
                            @else
                                {!! Form::open(['url' => '/critique']) !!}
                            @endif

                            {!! Form::hidden('produit_id', $produit->id) !!}

                            <p class="text-center">
                            <div class="form-group">
                                <div class="panel-body">
                                    <h2 class="no-margin">Album : <span>{{ $produit->nom }}</span></h2>
                                </div>
                                <div style="text-align: center" class="form-control">
                                    <label>Tapez votre
                                        critique
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="critique">
                                    {!! Form::textarea('commentaire') !!}
                                </div>
                            </div>
                            </p>
                        </div>
                        @if (count($errors) > 0)
                            <div class="panel-d">
                                <div class="panel-danger bg-danger">
                                    <ul style="color:red">
                                        @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif

                        <div class="panel-body">
                            <div style="text-align: center" class="form-control">


                                @if (isset($critique))
                                    <label>
                                        Modifiez votre ancien vote de {{ $critique->vote }} <i class="fa fa-star-o"
                                                                                               aria-hidden="true"></i>
                                    </label>
                                @else
                                    <label>
                                        Vote
                                    </label>
                                @endif

                            </div>
                        </div>

                        {{--Source rating: https://codepen.io/jamesbarnett/pen/vlpkh--}}


                        <div class="panel-body">
                            <img class="thumbnail-short" src="/img/{{$produit->cover}}.jpg"
                                 class="img-responsive">
                            <fieldset class="rating">
                                <input type="radio" id="star5" name="vote" value="5"/><label class="full" for="star5"
                                                                                             title="Awesome - 5 stars"></label>
                                <input type="radio" id="star4half" name="vote" value="4.5"/><label
                                        class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                                <input type="radio" id="star4" name="vote" value="4"/><label class="full" for="star4"
                                                                                             title="Pretty good - 4 stars"></label>
                                <input type="radio" id="star3half" name="vote" value="3.5"/><label
                                        class="half" for="star3half" title="Meh - 3.5 stars"></label>
                                <input type="radio" id="star3" name="vote" value="3"/><label class="full" for="star3"
                                                                                             title="Meh - 3 stars"></label>
                                <input type="radio" id="star2half" name="vote" value="2.5"/><label
                                        class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                                <input type="radio" id="star2" name="vote" value="2"/><label class="full" for="star2"
                                                                                             title="Kinda bad - 2 stars"></label>
                                <input type="radio" id="star1half" name="vote" value="1.5"/><label
                                        class="half" for="star1half" title="Meh - 1.5 stars"></label>
                                <input type="radio" id="star1" name="vote" value="1"/><label class="full" for="star1"
                                                                                             title="Sucks big time - 1 star"></label>
                                <input type="radio" id="starhalf" name="vote" value="half"/><label class="half"
                                                                                                   for="starhalf"
                                                                                                   title="Sucks big time - 0.5 stars"></label>
                            </fieldset>

                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                @if (isset($critique))
                                    <button type="submit" class="btn btn-danger">Modifier la critique</button>
                                @else
                                    <button type="submit" class="btn btn-danger">Publier la critique</button>
                                @endif
                            </div>
                        </div>

                    </div>

                </div>
                {!! Form::close() !!}


                <div class="col-xs-12 col-md-4">
                    <a href="/" type="button" class="btn btn-default btn-block btn-lg">
                        Accueil<br>
                        Mu<span class="logoMusic">z</span>i<span class="logoMusic">c</span>ity
                    </a>
                </div>

                <div class="col-xs-12 col-md-4">
                    <a href="/produits/all" type="button" class="btn btn-default btn-block btn-lg">
                        Nos<br>
                        Mu<span class="logoMusic">z</span>i<span class="logoMusic">c</span>
                    </a>
                </div>
                <div class="col-xs-12 col-md-4"><a href="{{ URL::previous() }}" class="btn btn-default btn-block btn-lg space-bottom">
                    Page précédente<br>
                    Mu<span class="logoMusic">z</span>i<span class="logoMusic">c</span>ity
                </a></div>


            </div>
        </div>
    </section>
@endsection
