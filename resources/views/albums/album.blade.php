@extends('layouts.app')

@section('title', 'Ajout album')
@section('content')
    <section id="addProduit">
        <div class="container">
            @if(Session::has('flash_message'))
                <div class="alert alert-info up text-center">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_message') }}
                </div>
            @endif
            <div class="row background-box">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel-default panel-primary">
                        <div class="panel-heading text-center">Nouvel Album</div>

                    @if (isset($produit))
                            {!! Form::model($produit,['method'=>'PUT', 'url' => 'produit/'.$produit->id]) !!}
                        @else
                            {!! Form::open(['url' => 'produit']) !!}
                        @endif
                        @if (count($errors) > 0)
                            <div class="panel-d">
                                <div class="panel-danger bg-danger">
                                    <ul style="color:red">
                                        @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-2 section">
                                        {!! Form::label('nom', 'Album') !!}
                                    </div>
                                    <div class="col-xs-10 col-sm-4 section">
                                        {!! Form::text('nom') !!}
                                    </div>

                                    <div class="col-xs-2 section">
                                        {!! Form::label('date', 'Année') !!}
                                    </div>
                                    <div class="col-xs-10 col-sm-4 section">
                                        {!! Form::text('date') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-2 section">
                                        {!! Form::label('synopsys', 'Synopsys') !!}
                                    </div>
                                    <div class="col-xs-10 section">
                                        {!! Form::textarea('synopsys') !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-2 section">
                                        {!! Form::label('auteurs', 'Auteur') !!}
                                    </div>
                                    <div class="col-xs-10 col-sm-4 section">
                                        {!! Form::text('auteurs') !!}
                                    </div>
                                    <div class="col-xs-2 section">
                                        {!! Form::label('cover', 'Cover') !!}
                                    </div>
                                    <div class="col-xs-10 col-sm-4 section">
                                        {!! Form::text('cover') !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2 section">
                                        {!! Form::label('classement_id', 'Classement') !!}
                                    </div>
                                    <div class="col-xs-10 col-sm-4 section">
                                        {!! Form::select('classement_id', array('Type de musique'=>array(
                                        '1' => 'Rock',
                                        '2' => 'Hard-rock',
                                        '3' => 'Pop',
                                        '4' => 'Ambiant',
                                        '5' => 'Electronique',
                                        '6' => 'Classique'))) !!}
                                    </div>
                                    <div class="col-xs-2 section">
                                        {!! Form::label('duree', 'Durée') !!}
                                    </div>
                                    <div class="col-xs-10 col-sm-4 section">
                                        {!! Form::text('duree') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-6 col-xs-offset-6">
                                        {!! Form::submit("Ajouter") !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
