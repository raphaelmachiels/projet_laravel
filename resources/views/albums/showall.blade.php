@extends('layouts.app')

@section('title', 'Tous les albums')
@section('content')
    <section id="showAll">
        <div class="container">
            <div class="row background-box">
                @include('includes.search')
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1>
                                @if($search)
                                    Recherche: {{ $nbProduit }} résultats
                                    @else
                                    Tous les albums
                                @endif
                            </h1>
                        </div>
                        <div class="panel-body">
                            @foreach($produits as $produit )
                                <li class="list-group-item clearfix">
                                    <div class="vignet left">
                                        <a href="/produit/{{ $produit->id }}"><img src="/img/{{$produit->cover}}.jpg"
                                                                                   class="img-responsive"></a>
                                    </div>
                                    <h4 class="left">{{ $produit->nom }}</h4>
                                    <a href="/produit/{{ $produit->id }}" class="right">
                                        <h4 class="showall-title">
                                            <i class="fa fa-fast-backward" aria-hidden="true"></i>
                                            {{ $produit->auteurs }}
                                            <i class="fa fa-fast-forward" aria-hidden="true"></i>
                                        </h4>
                                    </a>
                                </li>
                                    @foreach($classements as $classement)
                                    @if($classement->id == $produit->classement_id)
                                        <h4>Classement :</h4> {{ $classement->nom }}
                                    @endif
                                    @endforeach
                                <p><h4>Synopsys :</h4> {{ $produit->synopsys }}</p>
                                <p><h4>Durée :</h4> {{ $produit->duree }} minutes</p>
                                @if(Auth::user() && Auth::user()->role_id == 2)
                                    <div class="clearfix"><a href="/produit/{{ $produit->id }}/edit" type="button"
                                       class="btn btn-default btn-block btn-lg space-bottom click">
                                        Modifier
                                        cet album
                                    </a>
                                    {!! Form::open(['method' => 'DELETE', 'url' => '/produit/'.$produit->id]) !!}
                                    <input type="submit" value="Suprimer cet album"
                                       class="btn btn-default btn-block btn-lg space-bottom click"></div>
                                    {!! Form::close() !!}
                                @endif
                                <hr>
                            @endforeach


                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <a href="/" class="btn btn-default btn-block btn-lg space-bottom">
                        Accueil
                        Mu<span class="logoMusic">z</span>i<span class="logoMusic">c</span>ity
                    </a>
                </div>
                <div class="col-xs-12 col-md-4 col-md-offset-4">

                    <a href="{{ URL::previous() }}" class="btn btn-default btn-block btn-lg space-bottom">
                        Page précédente
                        Mu<span class="logoMusic">z</span>i<span class="logoMusic">c</span>ity
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection
