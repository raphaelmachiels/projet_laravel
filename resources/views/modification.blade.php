@extends('layouts.app')

@section('title', 'Ajout musiques')
@section('content')
    <section id="addProduit">
        <div class="container">
            <div class="row">
                <h1>L'album à bien été ajouté</h1>
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-primary">
                        <div class="panel-heading text-center">Nouvel Album</div>
                        {!! Form::open(['url' => 'produit']) !!}

                        <div class="panel-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-2 section">
                                        {!! Form::label('nom', 'Album') !!}
                                    </div>
                                    <div class="col-xs-10 col-sm-4 section">
                                        {!! Form::text('nom') !!}
                                    </div>

                                    <div class="col-xs-2 section">
                                        {!! Form::label('date', 'Année') !!}
                                    </div>
                                    <div class="col-xs-10 col-sm-4 section">
                                        {!! Form::text('date') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-2 section">
                                        {!! Form::label('synopsys', 'Synopsys') !!}
                                    </div>
                                    <div class="col-xs-10 section">
                                        {!! Form::textarea('synopsys') !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-2 section">
                                        {!! Form::label('auteurs', 'Auteur') !!}
                                    </div>
                                    <div class="col-xs-10 col-sm-4 section">
                                        {!! Form::text('auteurs') !!}
                                    </div>
                                    <div class="col-xs-2 section">
                                        {!! Form::label('cover', 'Cover') !!}
                                    </div>
                                    <div class="col-xs-10 col-sm-4 section">
                                        {!! Form::text('cover') !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2 section">
                                        {!! Form::label('classement', 'Classement') !!}
                                    </div>
                                    <div class="col-xs-10 col-sm-4 section">
                                        {!! Form::select('classement', array('Type de musique'=>array(
                                        '1' => 'Rock',
                                        '2' => 'Hard-rock',
                                        '3' => 'Pop',
                                        '4' => 'Ambiant',
                                        '5' => 'Electronique'))) !!}
                                    </div>
                                    <div class="col-xs-2 section">
                                        {!! Form::label('duree', 'Durée') !!}
                                    </div>
                                    <div class="col-xs-10 col-sm-4 section">
                                        {!! Form::text('duree') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-6 col-xs-offset-6">
                                        {!! Form::submit("Ajouter") !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}

                        @if (count($errors) > 0)
                        <ul style="color:red">
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
