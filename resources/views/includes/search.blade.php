    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-primary">
            <div class="panel-heading text-center welcom">
                <p class="heigh-search">Rechercher sur Mu<span class="logoMusic">z</span>i<span class="logoMusic">c</span>ity</p>
            </div>
            <div class="panel-body search">
                {!! Form::open(['method'=>'GET','action' => 'ProduitController@search']) !!}
                <?php
                echo Form::text('search');
                echo Form::submit('Go !');
                ?>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
