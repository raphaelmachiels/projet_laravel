<h3 class="text-center">
    <?php


    $nbStars = round($moyenneCritiques, 1);

    for ($star = 1; $star <= $nbStars; $star++) {
        echo '<i class="fa fa-star color-star" aria-hidden="true"></i>';
    }
    if (substr($nbStars, 2) >= 5) {
        echo '<i class="fa fa-star-half-o color-star" aria-hidden="true"></i>';
        $star++;
    }
    while ($star <= 5) {
        echo '<i class="fa fa-star-o color-star" aria-hidden="true"></i>';
        $star++;
    }
    ?>
</h3>

@if ($nbStars != 0)

    <h4 class="text-center">{{  $nbStars}} sur 5</h4>
        @else
             <h4 class="text-center">Nombre de votes insuffisant, votez !</h4>
    @endif
