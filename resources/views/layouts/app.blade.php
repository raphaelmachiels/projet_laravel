<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>MuZiCity - @yield('title')</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"
          integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link href="/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

</head>
<body id="app-layout">
<header>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Mu<span class="logoMusic">Z</span>i<span class="logoMusic">C</span>ity
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                {{--<ul class="nav navbar-nav">--}}
                    {{--<li><a href="{{ url('/home') }}">Home</a></li>--}}
                {{--</ul>--}}

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Se connecter</a></li>
                        <li><a href="{{ url('/register') }}">S'enregistrer</a></li>
                        <li><a href="{{ url('/produits/all') }}">Nos mu<span class="logoMenu">z</span>i<span
                                        class="logoMenu">c</span></a></li>
                    @else
                        @if(Auth::user()->role_id == 1)

                            <li><a href="{{ url('/produits/all') }}">Nos mu<span class="logoMenu">z</span>i<span
                                            class="logoMenu">c</span></a></li>

                            <li><a href="/critiques/{{Auth::user()->name}}/{{ Auth::user()->id }}">Mes cri<span
                                            class="logoMenu">t</span>i<span
                                            class="logoMenu">c</span>'s</a></li>
                        @endif

                        @if(Auth::user()->role_id == 2)

                            <li><a href="{{ url('/produits/all') }}">Nos mu<span class="logoMenu">Z</span>i<span
                                            class="logoMenu">C</span></a></li>

                            <li><a href="{{ url('produit/create') }}">Administration</a></li>
                        @endif
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Déconnexion</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
</header>

<main>


    @yield('content')
</main>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Corporis dolorem ex perspiciatis rem repellat!
                    Animi delectus hic, ipsam laborum mollitia numquam obcaecati.
                    Aut corporis enim, obcaecati odio pariatur perferendis repellendus.
                </p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Corporis dolorem ex perspiciatis rem repellat!
                    Animi delectus hic, ipsam laborum mollitia numquam obcaecati.
                    Aut corporis enim, obcaecati odio pariatur perferendis repellendus.
                </p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Corporis dolorem ex perspiciatis rem repellat!
                    Animi delectus hic, ipsam laborum mollitia numquam obcaecati.
                    Aut corporis enim, obcaecati odio pariatur perferendis repellendus.
                </p>
            </div>
            <div class="col-xs-12">
            </div>
        </div>
    </div>
    <div class="pied">
        <div class="container">
            Made By RMac &copy 2016
        </div>
    </div>

</footer>

<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"
        integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>
{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
